#include <string>
#include <iostream>
#include <memory>
#include "wheels.h"
#include "tire.h"
#include "engine.h"

using namespace std;

class car {
    string carbrand;
    int kmh;
    unique_ptr<itire> tire;
    unique_ptr<iwheels> wheels;
    unique_ptr<iengine> engine;
    public:
    car(string carbrand,
        unique_ptr<itire> tire,
        unique_ptr<iwheels> wheels,
        unique_ptr<iengine> engine) : 
        carbrand{carbrand}, 
        kmh{0},
        tire(move(tire)),
        wheels(move(wheels)),
        engine(move(engine)) {}

    void drive_city() {
        engine->on();
        kmh = 50;
        cout << "Fahren in der Stadt mit " << kmh << " kmh" << endl;
        engine->off();
    }
    void drive_highway() {
        engine->on();
        kmh = 160;
        cout << "Fahren auf der Autobahn mit " << kmh << " kmh" << endl;
        engine->off();
    }
    void drive_reverse() {
        engine->reverseOn();
        kmh = -10;
        cout << "Fahren rückwärts mit " << kmh << " kmh" << endl;
        engine->reverseOff();
    }
};

int main() {
    car* C = new car("BMW",unique_ptr<itire>(new Bridgestone()),unique_ptr<iwheels>(new BBS()),unique_ptr<iengine>(new diesel()));
    C->drive_city();
}