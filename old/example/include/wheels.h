#pragma once
#include <iostream>

class iwheels {
    public:
    virtual void information() = 0;
    virtual ~iwheels() {};
};

class OZ : public iwheels {
    void information(){
        std::cout << "Farbe der Felgen : schwarz" << std::endl;
    }
};

class BBS : public iwheels {
    void information() {
        std::cout << "Farbe der Felgen: silber" << std::endl;
    }
};