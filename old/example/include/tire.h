#pragma once
#include <iostream>

class itire {
    public:
    virtual void verbrauch() = 0;
    virtual ~itire() {};
};

class Michellin : public itire{
    void verbrauch(){
    std::cout << "gefahren wird mit Michellin-Reifen." <<
    " Verbrauch dieser Reifen beträgt ungefähr bei 66KW starkem Auto : 5.0L/100KM" << std::endl;
    }
};

class Bridgestone : public itire{
    void verbrauch(){
        std::cout << "gefahren wird mit Michellin-Reifen." <<
        " Verbrauch dieser Reifen beträgt ungefähr bei 66KW starkem Auto : 4.0L/100KM" << std::endl;
    }
};