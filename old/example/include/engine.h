#pragma once
#include <string>

class iengine {
    public:
    virtual void on() = 0;
    virtual void off() = 0;
    virtual ~iengine() {};
    void reverseOn(){
        std::cout << "Rückwartsfahren wird gestartet: Achte auf die Umgebung!" << std::endl; 
    }
    void reverseOff(){
        std::cout << "Das Rückwartsfahren wird deaktiviert" << std::endl;
    }
};

class diesel : public iengine {
    public:
    void on() {
        std::cout << "Dieselmotor hat gestartet zum Fahren" << std::endl;
    }
    void off() {
        std::cout << "Dieselmotor wird in Leergang gestellt" << std::endl;
    }
};

class benzin : public iengine {
    void on() {
        std::cout << "Benzinmotor ist bereit zum Fahren" << std::endl;
    }
    void off() {
        std::cout << "Benzinmotor wird in Leergang gestellt" << std::endl;
    }
};

class electro : public iengine {
    void on() {
        std::cout << "Elektromotor ist bereit zum Fahren" << std::endl;
    }
    void off() {
        std::cout << "Elektromotor wird ausgeschaltet" << std::endl;
    }
};